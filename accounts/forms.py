from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User



class SignUpForm(UserCreationForm):
	email = forms.CharField(widget = forms.EmailInput())
	dob = forms.DateField(widget = forms.DateInput(
			attrs = {'type': 'date'}
		))
	class Meta:
		model = User
		fields = ('email', 'dob', 'username', 'password1', 'password2')
