from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User

from django.http import Http404
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

#Generic view
from django.views.generic.base import TemplateView


from .models import Board, Post, Topic
from .forms import NewTopicForm
# Create your views here.

# //class based view



class HomeView(TemplateView):
	template_name = 'boards/home.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["boards"] = Board.objects.all() 
		return context
# def Home(request):
# 	board = Board.objects.all()
# 	return render(request, 'boards/home.html', {'boards':board})



class BoardTopicsView(TemplateView):
	template_name = 'boards/topics.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["board"] = get_object_or_404(Board, pk = self.kwargs['pk'])
		return context
# def board_topics(request, pk):
#     board = get_object_or_404(Board, pk = pk)
#     return render(request, 'boards/topics.html', {'board': board})


@method_decorator(login_required(login_url='/login/'), name='get')
class NewTopicView(TemplateView):
	template_name = 'boards/new_topic.html'

	def get(self, request, **kwargs):
		details = {
			'board': get_object_or_404(Board, pk=self.kwargs['pk']),
			'form': NewTopicForm()
		}
		return render(request, self.template_name, details)
	
	def post(self, request, *args, **kwargs):
		form = NewTopicForm(request.POST)
		board = get_object_or_404(Board, pk=self.kwargs['pk'])
		user = User.objects.first()
		if form.is_valid():
			topic = form.save(commit=False)
			topic.board=board
			topic.starter=user
			form.save()
			post = Post.objects.create(
					message=form.cleaned_data.get('message'),
	                topic=topic,
	                created_by=user
				)
			return redirect('board_topics', pk = board.pk)
		

# @login_required(login_url='/login/')
# def new_topic(request, pk):
# 	board = get_object_or_404(Board, pk=pk)
# 	user = User.objects.first()
# 	if request.method == 'POST':
# 		form = NewTopicForm(request.POST)
# 		if form.is_valid():
# 			topic = form.save(commit=False)
# 			topic.board=board
# 			topic.starter=user
# 			form.save()
# 			post=Post.objects.create(
# 					message=form.cleaned_data.get('message'),
# 	                topic=topic,
# 	                created_by=user
# 				)
# 			return redirect('board_topics', pk = board.pk)
# 	else:
# 		form = NewTopicForm()
 		
# 	return render(request, 'boards/new_topic.html', {'form': form, 'board':board})